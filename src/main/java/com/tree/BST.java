package com.tree;

/**
 * @author XiaoPan
 * date: 2022/1/18 20:08
 * <p>
 * action:  实现 二叉树
 */
public class BST {

    public static void main(String[] args) {

        BST BST = new BST();
        BST.insert(45);
        BST.insert(2);
        BST.insert(3);
        BST.insert(6);
        BST.insert(51);
        BST.insert(1);
        BST.insert(9);
        BST.insert(2);
        BST.insert(6);
        System.out.println();
    }


    //根节点
    private Node root;

    private int size = 0;


    /**
     * 插入 插入 成功 就 true
     */
    public void insert(int key) {
        if (root == null) {
            root = new Node(key);
            size++;
            return;
        }
        Node temp = root;

        Node node = new Node(key);

        while (true) {
            if (key > temp.date) {
                if (temp.right == null) {
                    temp.right = node;
                    size++;
                    return;
                }
                temp = temp.right;
                continue;
            }

            if (key < temp.date) {
                if (temp.left == null) {
                    temp.left = node;
                    size++;
                    return;
                }
                temp = temp.left;
                continue;
            }
            return;
        }
    }

    /**
     * 查找 有没有 这个数
     * 有的话就 return true
     */
    public boolean find(int key) {
        Node temp = root;
        while (temp != null) {
            if (key > temp.date) {
                temp = temp.right;
                continue;
            }

            if (key < temp.date) {
                temp = temp.left;
                continue;
            }
            return true;
        }
        return false;
    }


    /**
     * 删除
     */

    /**
     * 从小 到大 先序 中序 后序 遍历
     * <p>
     * 先序
     */

    public int[] priority() {
        int[] arr = new int[size];
        Node temp = root;

        return new int[1];
    }


    //二叉树 的 节点

    private class Node {
        int date;
        Node left;
        Node right;

        public Node(int date) {
            this.date = date;
        }
    }


}
