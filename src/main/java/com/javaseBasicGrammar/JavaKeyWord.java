package com.javaseBasicGrammar;

/**
 * 访问控制	            private	 protected	public
 *
 * 类，方法和变量修饰符	    abstract	class	extends	 final	implements	interface	native
 *  	                new	static	strictfp	synchronized	transient	volatile
 *
 * 程序控制	            break	continue	return	do	while	if	else
 *  	                for	instanceof	switch	case	default
 *
 * 错误处理	            try	catch	throw  throws	finally
 *
 * 包相关	            import	package
 *
 * 基本类型	            boolean	byte	char	double	float	int	long
 *  	                short	null	true	false
 *
 * 变量引用	            super	this	void
 *
 * 保留字	            goto  const
 *
 * 总计 53个   其中     synchronized    volatile  不怎么会用
 *                    strictfp  goto  const  transient  没见过
 *
 *
 *
 *                    volatile:
 *                              1. 用在多线程并发 修饰属性
 *                              2. 保证写进去的值和读出来的值是一致的
 *
 *
 * cpu 存储 会先存储到缓存中    使用volatile 就不会使用到缓存中
 *
 *
 */

public class JavaKeyWord {
}
