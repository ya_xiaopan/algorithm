package com.sort;

public interface Sort {
    int[] run(int[] arr);
}
