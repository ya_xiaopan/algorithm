package com.sort;

import java.util.Arrays;
import java.util.Random;

/**
 * @author XiaoPan
 * date: 2022/1/7 10:37
 * <p>
 * action:  测试各个排序的时间效率
 */
public class TextSort {

    public static void main(String[] args) {
//        Integer


//        System.out.println((int)(Math.random()*1)+1);

//        int min = 0;
//        int[] arr = new int[100*100*100*100];
//        int max = arr.length;
//
//
//        //填入数据给数组
//        for (int i = 0; i < arr.length; i++) {
//            arr[i] = (int) ((Math.random()*max));
//        }
//        Print.print(arr);





//        插入排序有问题
//        textSortTime(new InsertSort(), arr);

//        textSortTime(new BubbleSort(),arr);

//        int[] ints1 = textSortTime(new QuickSort2(), arr);


//        new Thread(()->{
//            textSortTime(new QuickSort(),arr);
//        }).start();
//        new Thread(()->{
//            textSortTime(new QuickSort2(),arr);
//        }).start();
//        new Thread(()->{
//            textSortTime(new QuickSort3(),arr);
//        }).start();



        while (true){
            int[] arr = new int[100*100*100*100];
            int max = arr.length;


            //填入数据给数组
            for (int i = 0; i < arr.length; i++) {
                arr[i] = (int) ((Math.random()*max));
            }
            System.out.println("----------------");
            textSortTime(new QuickSort(),arr);
            textSortTime(new QuickSort2(),arr);
            textSortTime(new QuickSort3(),arr);
            textSortTime(new QuickSort4(),arr);
            System.out.println();
        }

//        Print.print(ints);


//        textSortTime(new RadixSorting(),arr);
//        textSortTime(new SelectionSort(),arr);
    }

    public static int[] textSortTime(Sort sort, int[] arr) {
        int[] run = sort.run(arr);
//        Print.print(run);
        return run;
    }


}
