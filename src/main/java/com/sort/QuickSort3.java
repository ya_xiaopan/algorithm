package com.sort;

import java.util.Arrays;

/**
 * 快速排序
 */

public class QuickSort3 implements Sort {
    public static void main(String[] args) {
        int[] old1 = {1, 5, 6, 8, 9};
        int[] old = {18, 8, 20, 40, 0, 27, 8, 40, 0, 16, 40, 10, 2, 40, 5, 7, 27, 48, 40, 35, 44, 15, 47, 47, 42, 30, 26, 8, 34, 23, 8, 24, 40, 12, 3, 50, 50, 37, 27, 30, 10, 36, 33, 11, 9, 30, 44, 24, 7, 35};
        for (int i : old) {
            System.out.print(i + " ");
        }

        quickSort(old, 0, old.length - 1);
        System.out.println();

        for (int i : old) {
            System.out.print(i + " ");
        }
    }

    /**
     * 快速排序 Hoare法
     *
     * @param arr   待排序数组
     * @param left  左指针
     * @param right 右指针
     */

    public static void quickSort(int[] arr, int left, int right) {
        if (left >= right) {
            return;
        }
        if (right - left < 200) {
//            从第二个到最后一个都是待排序的
            for (int i = left + 1; i <= right; i++) {
//            记录插入的数据
                int temp = arr[i];

//            记录要插入的位置的index
                int j = i;

//            交换数据
                while (j > 0 && temp < arr[j - 1]) {
                    arr[j] = arr[j - 1];
                    j--;
                }

//          if为了稳定性
                if (j != i) {
                    arr[j] = temp;
                }
            }
            return;
        }

        int a = arr[left];
        int b = arr[(right - left) / 2];
        int c = arr[right];

        //取每次的第一个 中间的 最后一个 的中间值

        int max = Math.max(a, b);
        max = Math.max(c, max);//计算出三个数中的最大值

        int min = Math.min(a, b);
        min = Math.min(c, min);//计算出三个数中的最小值

        int d = a + b + c;//将三个数相加
        int key = d - max - min;//除去最大值和最小值
        int index = left;

        if (key == b) {
            index = (right - left) / 2;
            swap(arr, left, index);

        }
        if (key == c) {
            index = right;
            swap(arr, left, index);
        }

        //双指针，l左指针，r右指针
        int l = left, r = right;
        //选择数组第一个数作为基准值
        //先移动右指针
        while (l < r) {
            //移动右指针，寻找比基准值小的数
            while (l < r && arr[r] >= key) {
                r--;
            }
            //移动左指针，寻找比基准值大的数
            while (l < r && arr[l] <= key) {
                l++;
            }
            //交换找到的两个符合条件的值
            if (l < r) {
//                int temp = arr[l];
//                arr[l] = arr[r];
//                arr[r] = temp;
                swap(arr, l, r);
            }
        }
        //将基准值与指针位置数交换
        arr[left] = arr[l];
        arr[l] = key;
        //递归处理数组左边部分
        quickSort(arr, left, l - 1);
        //递归处理数组右边部分
        quickSort(arr, l + 1, right);
    }

    @Override
    public int[] run(int[] arr) {
        int[] ints = Arrays.copyOf(arr, arr.length);
        long l = System.currentTimeMillis();

        quickSort(ints, 0, ints.length - 1);

        System.out.println(QuickSort3.class.getName() + "的时间为:\t" + (System.currentTimeMillis() - l) + "ms");
        return arr;
    }


    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
