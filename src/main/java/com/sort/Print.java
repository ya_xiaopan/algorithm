package com.sort;

/**
 * @author XiaoPan
 * date: 2022/1/7 10:44
 * <p>
 * action:
 */
public class Print {
    public static void print(int[] arr){
        for (int i : arr) {
            System.out.print(i + ",");
        }
        System.out.println();
    }
}
