package com.sort;

import java.util.Arrays;

/**
 * 快速排序
 */

public class QuickSort implements Sort{
    public static void main(String[] args) {
//        int[] old1 = {1, 5, 6, 8, 9};
//        int[] old = {3, 19, 49, 45, 23, 78, 45, 12, 19, 461};


        int[] old = {20,66,55,-66,90,33,66,5151,696,10};
        System.out.print("排序前: ");
        for (int i : old) {
            System.out.print(i + "\t");
        }
        quickSort(old, 0, old.length - 1);
        System.out.println();
        System.out.print("排序后: ");
        for (int i : old) {
            System.out.print(i + "\t");
        }
    }

    /**
     * 快速排序 Hoare法
     *
     * @param arr 待排序数组
     * @param left 左指针
     * @param right 右指针
     */

    public static void quickSort(int[] arr, int left, int right) {
        if (left >= right) {
            return;
        }
        //双指针，l左指针，r右指针
        int l = left, r = right;
        //选择数组第一个数作为基准值
        int key = arr[l];
        //先移动右指针
        while (l < r) {
            //移动右指针，寻找比基准值小的数
            while (l < r && arr[r] >= key) {
                r--;
            }
            //移动左指针，寻找比基准值大的数
            while (l < r && arr[l] <= key) {
                l++;
            }

            //交换找到的两个符合条件的值
            if (l < r) {
                int temp = arr[l];
                arr[l] = arr[r];
                arr[r] = temp;
            }
        }
        //将基准值与指针位置数交换
        arr[left] = arr[l];
        arr[l] = key;
        //递归处理数组左边部分
        quickSort(arr, left, l - 1);
        //递归处理数组右边部分
        quickSort(arr, l + 1, right);
    }

    @Override
    public int[] run(int[] arr) {
        int[] ints = Arrays.copyOf(arr, arr.length);
        long l = System.currentTimeMillis();

        quickSort(ints, 0, ints.length - 1);

        System.out.println(QuickSort.class.getName() + "的时间为:\t" + (System.currentTimeMillis() - l) + "ms");
        return ints;
    }
}
