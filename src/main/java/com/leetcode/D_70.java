package com.leetcode;
/**
 * @author LongYu
 * @date 2022/7/28 9:58
 */
public class D_70 {
    public static void main(String[] args) {
        for (int i = 1; i < 47; i++) {
            int i1 = new D_70().climbStairs(i);
            System.out.println(i1);
        }
    }

    public int climbStairs(int n) {
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }

        if (n == 3) {
            return 3;
        }

        int a = 1;
        int b = 2;
        int c = 3;

        int i = 3;
        while (i < n) {
            a = b;
            b = c;
            c = a + b;
            i++;
        }
        return c;
    }
}
