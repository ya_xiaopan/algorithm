package com.leetcode;

import java.lang.reflect.Array;

/**
 * @author LongYu
 * @date 2022/7/13 10:46
 */
public class D_53 {
    public static void main(String[] args) {
////[-2,1,-3,4,-1,2,1,-5,4]
        int[] nums = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        int i = new D_53().max(nums);
        System.out.println(i);
    }


    public int max(int[] nums) {
        int res = nums[0];
        int sum = 0;
        for (int num : nums) {
            if (sum > 0)
                sum += num;
            else
                sum = num;
            res = Math.max(res, sum);
        }
        return res;
    }


    //    暴力 破解
    public int maxSubArray(int[] nums) {
        int len = nums.length;
        int[][] nu = new int[len][len];
        int max = Integer.MIN_VALUE;


        for (int i = 0; i < len; i++) {
            for (int j = 0; j <= i; j++) {
                if (j == 0) {
                    nu[j][i] = nums[i];
                } else {
                    nu[j][i] = nu[j - 1][i] + nu[0][i - j];
                }
                if (max < nu[j][i]) {
                    max = nu[j][i];
                }
            }
        }

        return max;
    }
}
