package com.leetcode;
/**
 * @author LongYu
 * @date 2022/7/28 17:21
 */
public class D_72 {
    public static void main(String[] args) {
        String a = "horse";
        String b = "ros";
        int i = new D_72().minDistance(a, b);
        System.out.println(i);
    }


    public int minDistance(String word1, String word2) {
        int length1 = word1.length();
        int length2 = word2.length();
        int[][] ints = new int[length1 + 1][length2 + 1];

        for (int i = 0; i <= length1; i++) ints[i][0] = i;
        for (int i = 0; i <= length2; i++) ints[0][i] = i;

        for (int i = 0; i < length1; i++) {
            for (int j = 0; j < length2; j++) {
                if (word1.charAt(i) == word2.charAt(j)) {
                    ints[i+1][j+1] = Math.min(Math.min(ints[i+1][j], ints[i][j+1]), ints[i][j] - 1) + 1;
                } else {
                    ints[i+1][j+1] = Math.min(Math.min(ints[i+1][j], ints[i][j+1]), ints[i][j]) + 1;
                }
            }
        }
        return ints[length1][length2];
    }
}