package com.leetcode;
/**
 * @author LongYu
 * @date 2022/7/27 11:11
 */
public class D_64 {
    public static void main(String[] args) {
//        [[1,3,1],[1,5,1],[4,2,1]]
        int[][] ints = {{1,3,1},{1,5,1},{4,2,1}};
        System.out.println(new D_64().minPathSum(ints));
    }


    public int minPathSum(int[][] grid) {
        int length = grid.length;
        int len = grid[0].length;

        for (int i = 0; i < grid.length-1; i++) {
            for (int i1 = 0; i1 < grid[i].length-1; i1++) {
                if (i == 0){
                    grid[i][i1+1] += grid[i][i1];
                }
                if (i1 == 0){
                    grid[i+1][i1] += grid[i][i1];
                }
            }

        }


        for (int i = 1; i < grid.length; i++) {
            for (int i1 = 1; i1 < grid[i].length; i1++) {
                grid[i][i1] += Math.min(grid[i-1][i1],grid[i][i1-1]);
            }
        }
        return grid[length-1][len-1];
    }
}
