package com.leetcode;

import java.util.Arrays;

/**
 * 00 03   i,j  j,len-i
 * 01 13
 * 10 02
 * <p>
 * 03 33
 * <p>
 * 33 30
 * <p>
 * 30 00
 */


public class D_48 {
    public static void main(String[] args) {
//        [1,2,3],[4,5,6],[7,8,9]
//        int[][] nums = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
//        [[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]
        int[][] nums = {{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}};
        int[][] nums1 = {{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}};


        new D_48().rotate(nums);
        System.out.println(Arrays.deepToString(nums));
        new D_48().rotate2(nums1);
        System.out.println(Arrays.deepToString(nums1));
    }

    //    旋转90度 也可以 先 先上下 在 对角交换
    public void rotate2(int[][] matrix) {
        int n = matrix.length;
        for (int i = 0; i < n / 2; i++) {
            int[] temp = matrix[i];
            matrix[i] = matrix[n - i - 1];
            matrix[n - i - 1] = temp;
        }
        for (int i = 0; i < n; i++) {
            for (int j = i;j<n;j++){
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
    }

    public void rotate(int[][] matrix) {
        int j = 2;
        if (matrix.length == 2) {
            j = 1;
        }
        for (int i = 0; i < matrix.length - j; i++) {
            for (int i1 = i; i1 < matrix.length - i - 1; i1++) {
                ro(matrix, i, i1, matrix[i][i1], 1);
            }
        }
    }


    //    旋转方法
    public void ro(int[][] matrix, int i, int j, int or, int k) {
        if (k == 1) {
            or = matrix[i][j];
        }
        if (k == 5) {
            return;
        }
        ro(matrix, j, matrix.length - i - 1, or, ++k);
        if (k == 2) {
            matrix[j][matrix.length - i - 1] = or;
        } else {
            matrix[j][matrix.length - i - 1] = matrix[i][j];
        }
    }
}
