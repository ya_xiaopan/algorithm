package com.leetcode;

/**
 * @author LongYu
 * @date 2022/7/18 15:13
 * <p>
 * 贪心算法
 */
public class D_55 {
    public static void main(String[] args) {
//        [2,3,1,1,4]
//        [3,2,1,0,4]
        int[] nums = {3, 2, 1, 0, 4};
        System.out.println(new D_55().canJump(nums));
    }

    public boolean canJump(int[] nums) {
        int len = nums.length;
        int m = 0;

        for (int i = 0; i < len - 1; i++) {
            m = Math.max(m, nums[i]);
            if (m > 0) {
                m--;
                if (m > len - i) {
                    return true;
                }
            } else {
                return false;
            }
        }
        return true;
    }
}