package com.leetcode;
import java.text.Collator;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;


public class Text {
    public static void main(String[] args) {
//        new Text().sort();
//        new Text().init();
        new Text().map();

    }

    public void map() {
        MapTest init = init();
        List<MapTestList> collect = init.getList()
                .stream()
                .peek((item) -> item.setAge(item.getAge() + 2)
                ).collect(Collectors.toList());
        init.setList(collect);

        System.out.println(init);

    }


    public MapTest init() {
        MapTestList ply = new MapTestList("ply", 18);
        MapTestList plyh = new MapTestList("plyh", 19);
        MapTestList palyh = new MapTestList("palyh", 19);
        MapTestList palwyh1 = new MapTestList("palwyh", 120);
        MapTestList palwyh2 = new MapTestList("palwyh", 122);
        MapTestList palwyh3 = new MapTestList("palwyh", 123);
        MapTestList palwyh4 = new MapTestList("palwyh", 121);
        MapTestList palwyh = new MapTestList("palwyh", 125);

        List<MapTestList> list = new ArrayList<>();
        list.add(ply);
        list.add(plyh);
        list.add(palyh);
        list.add(palwyh1);
        list.add(palwyh2);
        list.add(palwyh3);
        list.add(palwyh4);
        list.add(palwyh);
        return new MapTest("test", list);
//        System.out.println(test);
    }

    class MapTest {
        String title;
        List<MapTestList> list;

        public MapTest(String title, List<MapTestList> list) {
            this.title = title;
            this.list = list;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<MapTestList> getList() {
            return list;
        }

        public void setList(List<MapTestList> list) {
            this.list = list;
        }

        @Override
        public String toString() {
            return "MapTest{" +
                    "title='" + title + '\'' +
                    ", list=" + list +
                    '}';
        }
    }

    class MapTestList {
        String name;
        Integer age;

        public MapTestList(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "MapTestList{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }


    public void sort() {
        List<tes> list = new ArrayList<>();
        list.add(new tes("争议解决", "asadasd"));
        list.add(new tes("海关", "asadasd"));
        list.add(new tes("财富", "asadasd"));
        list.add(new tes("跨境", "asadasd"));
        list.add(new tes("投资", "asadasd"));
        list.add(new tes("知识", "asadasd"));
        list.add(new tes("资本", "asadasd"));
        List<tes> collect = list.stream()
                .sorted((x, y) -> Collator.getInstance(Locale.CHINA).compare(x.getTitle(), y.getTitle()))
                .collect(Collectors.toList());
        System.out.println(collect);
    }

    public void text2() {
        List<tes> list = new ArrayList<>();
        list.add(new tes("ac", "asadasd"));
        list.add(new tes("c", "asadasd"));
        list.add(new tes("r", "asadasd"));
        list.add(new tes("b", "asadasd"));
        list.add(new tes("d", "asadasd"));
        list.add(new tes("e", "asadasd"));
        list.add(new tes("f", "asadasd"));
        Collator instance = Collator.getInstance(Locale.CHINA);
        List<tes> collect = list.stream()
                .sorted((x, y) -> {
                    return instance.compare(x.getTitle(), y.getTitle());
                })
                .collect(Collectors.toList());


        System.out.println(collect);
    }


    class tes {
        String title;
        String path;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public tes(String title, String path) {
            this.title = title;
            this.path = path;
        }

        @Override
        public String toString() {
            return "tes{" +
                    "title='" + title + '\'' +
                    ", path='" + path + '\'' +
                    '}';
        }
    }
}
