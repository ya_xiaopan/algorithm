package com.leetcode;/*
    求出 全排列

 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class D_46 {
    public static void main(String[] args) {
        int[] num = {1, 2, 3};
        List<List<Integer>> permute = new D_46().permute(num);
        System.out.println(permute);
    }

    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        huis(nums, new ArrayList<>(), list, 0);
        return list;
    }


    public void huis(int[] nums, List<Integer> list, List<List<Integer>> lists, int i) {
        if (i == nums.length) {
            lists.add(new ArrayList<>(list));
            return;
        }
        for (int num : nums) {
            if (list.contains(num)) {
                continue;
            }
            list.add(num);
            huis(nums, list, lists, ++i);
            i--;
            list.remove(list.size() - 1);
        }
    }
}
