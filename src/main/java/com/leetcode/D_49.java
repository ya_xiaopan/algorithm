package com.leetcode;

import java.util.*;

public class D_49 {
    public static void main(String[] args) {
//        String[] strings = {"eat","tea","tan","ate","nat","bat"};
//        "hhhhu","tttti","tttit","hhhuh","hhuhh","tittt"]
        String[] strings = {"hhhhu", "tttti", "tttit", "hhhuh", "hhuhh", "tittt"};
//        String[] strings = {"", "b", ""};
//        String[] strings = {"ac","c"};

        List<List<String>> lists = new D_49().groupAnagrams(strings);
        System.out.println(lists);
    }


    public List<List<String>> groupAnagrams(String[] strs) {
//        paixu
        Map<String, List<String>> map = new HashMap<>();
        for (String str : strs) {
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String key = new String(chars);
            List<String> orDefault = map.getOrDefault(key, new ArrayList<>());
            orDefault.add(str);
            map.put(key,orDefault);
        }
      return new ArrayList<>(map.values());
    }
}

