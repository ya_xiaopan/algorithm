package com.hashmap;

import com.hashmap.test01.TestMap1;

/**
 * @author XiaoPan
 * date: 2022/1/17 17:30
 * <p>
 * action:
 */
public class test {
    public static void main(String[] args) {
        TestMap1<Integer, Integer> objectObjectTestMap = new TestMap1<>();


//        objectObjectTestMap.put(1, 1);
//        objectObjectTestMap.put(17, 17);
//        objectObjectTestMap.put(33, 33);
//        objectObjectTestMap.put(49, 49);
//        objectObjectTestMap.put(65, 65);
//        objectObjectTestMap.put(3, 3);
//        objectObjectTestMap.put(4, 4);
//
//
//        Integer remove = objectObjectTestMap.remove(17);
//        System.out.println(remove);
//
//        System.out.println(objectObjectTestMap.get(17));
//        System.out.println(objectObjectTestMap.get(49));

//        objectObjectTestMap.put(11, 11);
//        objectObjectTestMap.put(12, 12);
//        objectObjectTestMap.put(13, 13);
//        objectObjectTestMap.put(14, 14);
//
//        objectObjectTestMap.put(25, 25);
//        objectObjectTestMap.put(50, 50);
//        objectObjectTestMap.put(75, 75);
        objectObjectTestMap.put(100 * 100 * 100 + 1, 76);

//
        long l = System.currentTimeMillis();
        for (int i = 0; i < 100 * 100 * 100; i++) {
            int v = (int) (Math.random() * 100 * 100 * 100);
            objectObjectTestMap.put(v, v);
        }
        System.out.println("构造时花费了:  " + (System.currentTimeMillis() - l));

        long l1 = System.currentTimeMillis();
        System.out.println(objectObjectTestMap.get(95595));
        System.out.println(objectObjectTestMap.size());
        System.out.println("查找时花费了:  " + (System.currentTimeMillis() - l1));
    }
}
